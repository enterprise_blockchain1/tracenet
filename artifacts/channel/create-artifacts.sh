# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/hlf_producetrace/fabric-samples/bin" ] ; then

PATH="/workspaces/hlf_producetrace/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
# rm -rf ./crypto-config
# rm genesis.block mychannel.tx
# rm -rf ../../channel-artifacts/*

rm tracenet-genesis.block tracenet-channel.tx





#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/


# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD



# Generate the genesis block for the University Consortium Orderer
configtxgen -profile TraceNetOrdererGenesis -channelID ordererchannel -outputBlock tracenet-genesis.block


# Create the channel TraceNetChannel
configtxgen -outputCreateChannelTx ./tracenet-channel.tx -profile TraceNetChannel -channelID tracenetchannel

# create anchor peers
configtxgen -outputAnchorPeersUpdate Org1MSPAnchors.tx -profile TraceNetChannel -channelID tracenetchannel -asOrg Org1 